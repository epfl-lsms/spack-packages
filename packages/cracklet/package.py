# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class Cracklet(CMakePackage):
    """
    cRacklet is an opensource object-oriented spectral boundary integral
    method library aiming at simulating interfacial rupture.
    """

    git = "https://gitlab.com/cracklet/cracklet.git"

    maintainers = ["tiburoch"]

    version('master', branch='master')

    variant('openmp',default=True,
            description="Activates multithreaded capabilities")
    variant('python', default=False,
            description="Activates python bindings")

    depends_on('boost')
    depends_on('fftw')
    depends_on('gsl')

    depends_on('cmake@3.1.3:', type='build')
    depends_on('python',when='+python', type=('build', 'run'))
    depends_on('py-pybind11', when='@3.1:+python', type=('build', 'run'))

    extends('python', when='+python')

    def cmake_args(self):
        spec = self.spec

        args = [
            '-DCRACKLET_MULTI_THREADED:BOOL={0}'.format(
                'ON' if spec.satisfies('+openmp') else 'OFF'),
            '-DCRACKLET_PYTHON_INTERFACE:BOOL={0}'.format(
                'ON' if spec.satisfies('+python') else 'OFF')
        ]

        return args
